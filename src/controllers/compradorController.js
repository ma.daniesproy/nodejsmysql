const controller={};

controller.list=(req, res)=>{
    req.getConnection((err,conn)=>{
        conn.query('SELECT * FROM comprador',(err,rows)=>{
            if (err) {
                res.json(err); 
            } 
            console.log(rows);
            res.render('compradores',{
                data:rows
            });
        });
    });
}
controller.mostrar=(req, res)=>{
    const id=req.params.id;
    req.getConnection((err,conn)=>{
        conn.query('SELECT * FROM comprador WHERE id= ?',[id],(err,rows)=>{
            if (err) {
                res.json(err); 
            } 
            console.log(rows);
            res.render('compradoresMostrar',{
                data:rows[0]
            });
        });
    });
}

controller.save=(req, res)=>{
    const data=req.body;
    req.getConnection((err, conn)=>{
        conn.query('INSERT INTO comprador set ?',[data],(err,comprador)=>{
            //console.log(comprador);
            res.redirect('/');
        });
    });
}

controller.edit=(req, res)=>{
    const id=req.params.id;
    req.getConnection((err,conn)=>{
        conn.query('SELECT * FROM comprador WHERE id= ?',[id],(err,row)=>{
            console.log(row[0]);
            res.render('compradoresEdit',{
                data:row[0]
            });
        });
    });
}

controller.actualizar=(req, res)=>{
    const id=req.params.id;
    const nuevoComprador=req.body;
    req.getConnection((err,conn)=>{
        conn.query('UPDATE comprador SET ? WHERE id= ?',[nuevoComprador,id],(err,row)=>{
            res.redirect('/');
        });
    });
}

controller.delete=(req, res)=>{
    const id=req.params.id;
    req.getConnection((err, conn)=>{
        conn.query('DELETE FROM comprador WHERE id= ?',[id],(err,result)=>{
            res.redirect('/');
        });
    });
}

module.exports=controller;