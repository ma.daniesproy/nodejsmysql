CREATE DATABASE nodemysql;

USE nodemysql

CREATE TABLE comprador(
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(30) NOT NULL,
    paterno VARCHAR(30) NOT NULL,
    materno VARCHAR(30) NOT NULL,
    sexo set("F","M") NOT NULL,
    edad int(3) NOT NULL,
    telefono VARCHAR(10)NOT NULL,
    calle VARCHAR(60)NOT NULL,
    noExt VARCHAR(12)NOT NULL,
    noInt VARCHAR(12)NOT NULL,
    orientacion SET("Norte","Poniente","Oriente","Sur","Desconocido") NOT NULL
);

SHOW TABLES;

DESC comprador;