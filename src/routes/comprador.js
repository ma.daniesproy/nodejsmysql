const express=require('express');
const router=express.Router();
const compradorController=require('../controllers/compradorController') ;

router.get('/',compradorController.list);

router.post('/agregar',compradorController.save);
router.get('/eliminar/:id',compradorController.delete);
router.get('/actualizar/:id',compradorController.edit);
router.post('/actualizar/:id',compradorController.actualizar);
router.get('/mostrar/:id',compradorController.mostrar);

module.exports=router;