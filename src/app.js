const express=require('express');
const path=require('path');
const morgan=require('morgan');
const mysql=require('mysql');
const myConnection=require('express-myconnection');
const app=express();

//IMPORTANDO ROUTERS
const compradorRouters=require('./routes/comprador');
const { urlencoded } = require('express');

//SETINGS
app.set('port', process.env.PORT|| 3000);
app.set('view engine','ejs');
app.set('views',path.join(__dirname,'views'));

//MIDDLEWARES
app.use(morgan('dev')); 
app.use(myConnection(mysql, {
    host:'localhost',
    user:'maria',
    password:'#Maria3224',
    port:3306,
    database:'nodemysql'
},'single'));

app.use(express.urlencoded({
    extended:false|true
}));

//ROUTES
app.use('/',compradorRouters);


//STATIC FILES
app.use(express.static(path.join(__dirname, 'public')));

//STAR THE SERVER
app.listen(app.get('port'),()=>{
    console.log("RUNNING IN THE SERVER 3000");
})